﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tile : MonoBehaviour
{
    public Point GridPosition { private set; get; }
    public Vector2 WorldPosition
    {
        get
        {
            SpriteRenderer renderer = GetComponent<SpriteRenderer>();
            return new Vector2(transform.position.x + renderer.bounds.size.x / 2, transform.position.y - renderer.bounds.size.y / 2);
        }
    }

    public bool IsEmpty { get; set; }

    private SpriteRenderer spriteRenderer;
    private Color32 normalColor = new Color32(255, 255, 255, 255);
    private Color32 fullColor = new Color32(255, 118, 118, 255);
    private Color32 emptyColor = new Color32(96, 200, 90, 255);
    private Tower placedTower;

    // Start is called before the first frame update
    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Setup(Point gridPosition, Vector3 worldPosition, Transform parent)
    {
        this.GridPosition = gridPosition;
        this.transform.position = worldPosition;
        IsEmpty = true;
        transform.parent = parent;
        LevelManager.Instance.Tiles.Add(gridPosition, this);
    }

    private void OnMouseOver()
    {
        if (!EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedButton != null)
        {
            if (IsEmpty)
            {
                ColorTile(emptyColor);

                if (Input.GetMouseButtonDown(0))
                {
                    PlaceTower();
                }
            }
            else
            {
                ColorTile(fullColor);
            }
        }
        else if (!EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedButton == null && Input.GetMouseButtonDown(0))
        {
            if (placedTower != null)
            {
                GameManager.Instance.SelectTower(placedTower);
            }
            else
            {
                GameManager.Instance.DeSelectTower();
            }
        }
    }

    private void OnMouseExit()
    {
        if (GameManager.Instance.ClickedButton != null)
        {
            ColorTile(normalColor);
        }
    }

    private void PlaceTower()
    {
        IsEmpty = false;

        if (AStar.GetPath(LevelManager.Instance.GreenSpawnPoint, LevelManager.Instance.PurpleSpawnPoint) == null)
        {
            // Tower will block the monster road
            IsEmpty = true;
            return;
        }
        
        GameObject tower = Instantiate(GameManager.Instance.ClickedButton.TowerPrefab, WorldPosition, Quaternion.identity);
        SpriteRenderer[] renderers = tower.GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer renderer in renderers)
        {
            renderer.sortingOrder = this.GridPosition.Y;
        }

        tower.transform.SetParent(transform);
        placedTower = tower.transform.Find("Range").GetComponent<Tower>();
        placedTower.Price = GameManager.Instance.ClickedButton.Price;
        GameManager.Instance.BuyTower();
        ColorTile(normalColor);
    }

    private void ColorTile(Color32 newColor)
    {
        this.spriteRenderer.color = newColor;
    }
}