﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class AStar
{
    private static Dictionary<Point, Node> nodes;

    private static void CreateNodes()
    {
        nodes = new Dictionary<Point, Node>();

        foreach (Tile tile in LevelManager.Instance.Tiles.Values)
        {
            nodes.Add(tile.GridPosition, new Node(tile));
        }
    }

    public static Stack<Node> GetPath(Point start, Point goal)
    {
        if (nodes == null)
        {
            CreateNodes();
        }

        HashSet<Node> openList = new HashSet<Node>();
        HashSet<Node> closedList = new HashSet<Node>();
        Stack<Node> path = new Stack<Node>();

        Node currentNode = nodes[start];
        openList.Add(currentNode);

        while (openList.Count != 0)
        {
            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    Point neighbourPos = new Point(currentNode.GridPosition.X - x, currentNode.GridPosition.Y - y);

                    int gCost = 0;
                    if (Mathf.Abs(x - y) == 1)
                    {
                        gCost = 10;
                    }
                    else
                    {
                        if (LevelManager.Instance.InBounds(neighbourPos) && !ConnectedDiagonally(currentNode, nodes[neighbourPos]))
                        {
                            continue;
                        }

                        gCost = 14;
                    }

                    if (LevelManager.Instance.InBounds(neighbourPos) && !neighbourPos.Equals(currentNode.GridPosition) && LevelManager.Instance.Tiles[neighbourPos].IsEmpty)
                    {
                        Node neighbourNode = nodes[neighbourPos];

                        if (openList.Contains(neighbourNode))
                        {
                            if (currentNode.GScore + gCost < neighbourNode.GScore)
                            {
                                neighbourNode.CalculateValues(currentNode, nodes[goal], gCost);
                            }
                        }
                        else if (!closedList.Contains(neighbourNode))
                        {
                            openList.Add(neighbourNode);
                            neighbourNode.CalculateValues(currentNode, nodes[goal], gCost);
                        }
                    }
                }
            }

            openList.Remove(currentNode);
            closedList.Add(currentNode);

            if (openList.Count > 0)
            {
                currentNode = openList.OrderBy(n => n.FScore).First();
            }

            if (currentNode.Equals(nodes[goal]))
            {
                while (!currentNode.GridPosition.Equals(start))
                {
                    path.Push(currentNode);
                    currentNode = currentNode.Parent;
                }

                return path;
            }
        }

        return null;
        // GameObject.Find("AStarDebugger").GetComponent<AStarDebugger>().DebugPath(openList, closedList, path);
    }

    private static bool ConnectedDiagonally(Node currentNode, Node neighbourNode)
    {
        Point direction = neighbourNode.GridPosition - currentNode.GridPosition;

        Point first = new Point(currentNode.GridPosition.X + direction.X, currentNode.GridPosition.Y);
        Point second = new Point(currentNode.GridPosition.X, currentNode.GridPosition.Y + direction.Y);

        if (LevelManager.Instance.InBounds(first) && !LevelManager.Instance.Tiles[first].IsEmpty)
        {
            return false;
        }

        if (LevelManager.Instance.InBounds(second) && !LevelManager.Instance.Tiles[second].IsEmpty)
        {
            return false;
        }

        return true;
    }
}