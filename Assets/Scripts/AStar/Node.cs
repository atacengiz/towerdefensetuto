﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    private Tile tile;
    private Node parent;
    private int gScore;
    private int hScore;
    private int fScore;

    public Tile Tile { get => tile; }
    public Point GridPosition { get => tile.GridPosition; }
    public Node Parent { get => parent; }
    public int GScore { get => gScore; }
    public int HScore { get => hScore; }
    public int FScore { get => fScore; set => fScore = value; }
    public Vector3 WorldPosition { get; set; }

    public Node(Tile tile)
    {
        this.tile = tile;
        this.WorldPosition = tile.WorldPosition;
    }

    public void CalculateValues(Node parent, Node goal, int gCost)
    {
        this.parent = parent;
        this.gScore = parent.gScore + gCost;

        int horizontalHDifference = Mathf.Abs(goal.GridPosition.X - this.GridPosition.X) * 10;
        int verticalHDifference = Mathf.Abs(goal.GridPosition.Y - this.GridPosition.Y) * 10;
        this.hScore = horizontalHDifference + verticalHDifference;
        this.fScore = this.gScore + this.hScore;
    }
}
