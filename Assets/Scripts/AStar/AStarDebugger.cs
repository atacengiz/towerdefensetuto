﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AStarDebugger : MonoBehaviour
{

    private Tile start;
    private Tile goal;
    [SerializeField] private GameObject arrowPrefab;
    [SerializeField] private GameObject debugTilePrefab;

    // private void Update()
    // {
    //     if (Input.GetMouseButtonDown(1))
    //     {
    //         ClickTile();
    //     }

    //     if (Input.GetKeyDown(KeyCode.Space))
    //     {
    //         AStar.GetPath(start.GridPosition, goal.GridPosition);
    //     }
    // }

    private void ClickTile()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            Tile hitTile = hit.collider.GetComponent<Tile>();

            if (hitTile != null)
            {
                if (start == null)
                {
                    this.start = hitTile;
                    PlaceDebugTile(start.WorldPosition, new Color32(255, 135, 0, 255));
                }
                else if (goal == null)
                {
                    this.goal = hitTile;
                    PlaceDebugTile(goal.WorldPosition, new Color32(255, 0, 0, 255));
                }
            }
        }
    }

    public void DebugPath(HashSet<Node> openList, HashSet<Node> closedList, Stack<Node> path)
    {
        foreach (Node node in openList)
        {
            if (!node.Tile.Equals(start) && !node.Tile.Equals(goal))
            {
                PlaceDebugTile(node.Tile.WorldPosition, Color.cyan, node);
            }

            PointToParent(node, node.Tile.WorldPosition);
        }

        foreach (Node node in closedList)
        {
            if (!node.Tile.Equals(start) && !node.Tile.Equals(goal) && !path.Contains(node))
            {
                PlaceDebugTile(node.Tile.WorldPosition, Color.blue, node);
            }

            PointToParent(node, node.Tile.WorldPosition);
        }

        foreach (Node node in path)
        {
            if (!node.Tile.Equals(start) && !node.Tile.Equals(goal))
            {
                PlaceDebugTile(node.Tile.WorldPosition, Color.green, node);
            }
        }
    }

    private void PointToParent(Node node, Vector2 position)
    {
        if (node.Parent != null)
        {
            GameObject arrow = Instantiate(arrowPrefab, position, Quaternion.identity);

            if (node.GridPosition.X < node.Parent.GridPosition.X && node.GridPosition.Y == node.Parent.GridPosition.Y) // Left side
            {
                arrow.transform.eulerAngles = Vector3.zero;
            }
            else if (node.GridPosition.X > node.Parent.GridPosition.X && node.GridPosition.Y == node.Parent.GridPosition.Y) // Right side
            {
                arrow.transform.eulerAngles = new Vector3(0, 0, 180);
            }
            else if (node.GridPosition.X == node.Parent.GridPosition.X && node.GridPosition.Y < node.Parent.GridPosition.Y) // Top side
            {
                arrow.transform.eulerAngles = new Vector3(0, 0, 270);
            }
            else if (node.GridPosition.X == node.Parent.GridPosition.X && node.GridPosition.Y > node.Parent.GridPosition.Y) // Bottom side
            {
                arrow.transform.eulerAngles = new Vector3(0, 0, 90);
            }
            else if (node.GridPosition.X < node.Parent.GridPosition.X && node.GridPosition.Y > node.Parent.GridPosition.Y) // Upper left side
            {
                arrow.transform.eulerAngles = new Vector3(0, 0, 45);
            }
            else if (node.GridPosition.X > node.Parent.GridPosition.X && node.GridPosition.Y > node.Parent.GridPosition.Y) // Upper right side
            {
                arrow.transform.eulerAngles = new Vector3(0, 0, 135);
            }
            else if (node.GridPosition.X < node.Parent.GridPosition.X && node.GridPosition.Y < node.Parent.GridPosition.Y) // Bottom left side
            {
                arrow.transform.eulerAngles = new Vector3(0, 0, 315);
            }
            else if (node.GridPosition.X > node.Parent.GridPosition.X && node.GridPosition.Y < node.Parent.GridPosition.Y) // Bottom right side
            {
                arrow.transform.eulerAngles = new Vector3(0, 0, 225);
            }
        }
    }

    private void PlaceDebugTile(Vector3 worldPosition, Color32 color, Node node = null)
    {
        GameObject debugTile = Instantiate(debugTilePrefab, worldPosition, Quaternion.identity);
        if (node != null)
        {
            DebugTile tile = debugTile.GetComponent<DebugTile>();
            tile.GText.text = "G:" + node.GScore.ToString();
            tile.HText.text = "H:" + node.HScore.ToString();
            tile.FText.text = "F:" + node.FScore.ToString();
        }

        debugTile.GetComponent<SpriteRenderer>().color = color;
    }
}
