﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : Singleton<Hover>
{
    private SpriteRenderer spriteRenderer;
    private SpriteRenderer rangeSpriteRenderer;
    
    public bool IsVisible { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;

        rangeSpriteRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
        rangeSpriteRenderer.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.spriteRenderer.enabled)
        {
            FollowMouse();
        }
    }

    private void FollowMouse()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(mousePosition.x, mousePosition.y, 0.0f);
    }

    public void Activate(Sprite sprite)
    {
        this.spriteRenderer.sprite = sprite;
        this.spriteRenderer.enabled = true;
        this.rangeSpriteRenderer.enabled = true;
        IsVisible = true;
    }

    public void DeActivate()
    {
        this.spriteRenderer.enabled = false;
        this.rangeSpriteRenderer.enabled = false;
        IsVisible = false;
    }
}
