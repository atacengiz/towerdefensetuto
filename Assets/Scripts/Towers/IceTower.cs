﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceTower : Tower
{
    [field: SerializeField] public float SlowingFactor { get; private set; }

    private void Start()
    {
        base.Start();
        this.Element = Element.FROST;
        Upgrades = new TowerUpgrade[]
        {
            new TowerUpgrade(2, 1, 1.0f, 2, 10),
            new TowerUpgrade(2, 1, 1.0f, 2, 20)
        };
    }

    public override Debuff GetDebuff()
    {
        return new FrostDebuff(SlowingFactor, target, DebuffDuration);
    }
    
    public override string GetStats()
    {
        if (NextUpgrade != null)  //If the next is avaliable
        {
            return string.Format("<color=#00ffffff>{0}</color>{1} \nSlowing factor: {2}% <color=#00ff00ff>+{3}</color>", "<size=20><b>Frost</b></size>", base.GetStats(), SlowingFactor, NextUpgrade.SlowingFactor);
        }
 
        return string.Format("<color=#00ffffff>{0}</color>{1} \nSlowing factor: {2}%", "<size=20><b>Frost</b></size>", base.GetStats(), SlowingFactor);
    }

    public override void Upgrade()
    {
        this.SlowingFactor += NextUpgrade.SlowingFactor;

        base.Upgrade();
    }
}
