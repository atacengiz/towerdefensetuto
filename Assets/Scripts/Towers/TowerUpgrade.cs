﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerUpgrade
{
    public int Price { get; private set; }
    public int Damage { get; private set; }
    public float DebuffDuration { get; private set; }
    public float ProcChance { get; private set; }
    public float SlowingFactor { get; private set; }
    public float TickTime { get; private set; }
    public float SpecialDamage { get; private set; }
    
    public TowerUpgrade(int price, int damage, float debuffDuration, float procChance)
    {
        Price = price;
        Damage = damage;
        DebuffDuration = debuffDuration;
        ProcChance = procChance;
    }

    public TowerUpgrade(int price, int damage, float debuffDuration, float procChance, float slowingFactor)
    {
        Price = price;
        Damage = damage;
        DebuffDuration = debuffDuration;
        ProcChance = procChance;
        SlowingFactor = slowingFactor;
    }
    
    public TowerUpgrade(int price, int damage, float debuffDuration, float procChance, float tickTime, float specialDamage)
    {
        Price = price;
        Damage = damage;
        DebuffDuration = debuffDuration;
        ProcChance = procChance;
        TickTime = tickTime;
        SpecialDamage = specialDamage;
    }
}
