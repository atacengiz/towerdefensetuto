﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Element
{
    STORM,
    FIRE,
    FROST,
    POISON,
    NONE
}

public abstract class Tower : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    protected Monster target;
    private Queue<Monster> targets;
    private bool canAtack = true;
    private Animator animator;
    private float attackTimer;
    [SerializeField] private int damage;
    [SerializeField] private float attackCooldown;
    [SerializeField] private string projectileType;
    [SerializeField] private float projectileSpeed;
    [SerializeField] private Transform projectileTransform;
    [SerializeField] private float debuffDuration;
    [SerializeField] private float proc;

    public float ProjectileSpeed { get => projectileSpeed; set => projectileSpeed = value; }
    public int Damage { get => damage; }
    public int Price { get; set; }
    public Element Element { get; protected set; }
    public float DebuffDuration { get => debuffDuration; }
    public float Proc { get => proc; }
    public TowerUpgrade[] Upgrades { get; protected set; }
    public int CurrrentTowerLevel { get; protected set; }

    public TowerUpgrade NextUpgrade
    {
        get
        {
            if (Upgrades.Length > CurrrentTowerLevel - 1)
            {
                return Upgrades[CurrrentTowerLevel - 1];
            }
            else
            {
                return null;
            }
        }
    }

    protected void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponentInParent<Animator>();
        CurrrentTowerLevel = 1;
    }

    protected void Start()
    {
        targets = new Queue<Monster>();
        attackTimer = 0.0f;
    }

    protected void Update()
    {
        Atack();
    }

    public void Select()
    {
        spriteRenderer.enabled = !spriteRenderer.enabled;
        GameManager.Instance.UpdateUpgradeTooltip();
    }

    private void Atack()
    {
        if (!canAtack)
        {
            attackTimer += Time.deltaTime;

            if (attackTimer >= attackCooldown)
            {
                canAtack = true;
                attackTimer = 0.0f;
            }
        }

        if (this.target == null && targets.Count > 0 && targets.Peek().IsActive)
        {
            target = targets.Dequeue();
        }
        if (this.target != null && this.target.IsActive)
        {
            if (this.canAtack)
            {
                Shoot();
                animator.SetTrigger("AttackTrigger");

                canAtack = false;
            }
        }
        if (this.target != null && !this.target.IsAlive || this.target != null && !this.target.IsActive)
        {
            target = null;
        }
    }

    public abstract Debuff GetDebuff();

    public virtual string GetStats()
    {
        if (NextUpgrade != null)
        {
            return string.Format("\nLevel: {0} \nDamage: {1} <color=#00ff00ff> +{4}</color>\nProc: {2}% <color=#00ff00ff>+{5}%</color>\nDebuff: {3}sec <color=#00ff00ff>+{6}</color>", CurrrentTowerLevel, damage, proc, DebuffDuration, NextUpgrade.Damage, NextUpgrade.ProcChance, NextUpgrade.DebuffDuration);
        }
 
        return string.Format("\nLevel: {0} \nDamage: {1} \nProc: {2}% \nDebuff: {3}sec", CurrrentTowerLevel, damage, proc, DebuffDuration);
    }

    public virtual void Upgrade()
    {
        GameManager.Instance.Currency -= NextUpgrade.Price;
        Price += NextUpgrade.Price;
        this.damage += NextUpgrade.Damage;
        this.proc += NextUpgrade.ProcChance;
        this.debuffDuration += NextUpgrade.DebuffDuration;
        this.CurrrentTowerLevel++;
        GameManager.Instance.UpdateUpgradeTooltip();
    }

    private void Shoot()
    {
        Projectile projectile = GameManager.Instance.ObjectPool.GetObject(projectileType).GetComponent<Projectile>();
        projectile.transform.position = projectileTransform.position;

        projectile.Init(this, this.target);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Monster"))
        {
            Monster monster = other.GetComponent<Monster>();
            this.targets.Enqueue(monster);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Monster"))
        {
            this.target = null;
        }
    }
}
