﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StormTower : Tower
{
    private void Start()
    {
        base.Start();
        this.Element = Element.STORM;
        
        Upgrades = new TowerUpgrade[]
        {
            new TowerUpgrade(5, 1, 1, 2),
            new TowerUpgrade(5, 3, 1, 2)
        };
    }

    public override Debuff GetDebuff()
    {
        return new StormDebuff(target, DebuffDuration);
    }

    public override string GetStats()
    {
        return string.Format("<color=#add8e6ff>{0}</color>{1}", "<size=20><b>Storm</b></size>", base.GetStats());
    }
}