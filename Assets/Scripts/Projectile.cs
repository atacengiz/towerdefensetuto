﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Monster target;
    private Tower parentTower;
    private Animator animator;
    private Element elementType;

    private void Awake()
    {
        this.animator = GetComponent<Animator>();
    }

    public void Init(Tower parentTower, Monster target)
    {
        this.parentTower = parentTower;
        this.target = target;
        this.elementType = parentTower.Element;
    }

    // Update is called once per frame
    void Update()
    {
        MoveToTarget();
    }

    public void MoveToTarget()
    {
        if (target != null && target.IsActive)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, parentTower.ProjectileSpeed * Time.deltaTime);

            Vector2 direction = target.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
        else if (!target.IsActive)
        {
            GameManager.Instance.ObjectPool.ReleaseObject(gameObject);
        }
    }

    private void ApplyDebuff()
    {
        if (target.ElementType != elementType)
        {
            float roll = Random.Range(0, 100);
            if (roll <= parentTower.Proc)
            {
                target.AddDebuff(parentTower.GetDebuff());
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Monster"))
        {
            Monster hitInfo = other.GetComponent<Monster>();
            if (target.gameObject == hitInfo.gameObject)
            {
                hitInfo.ModifyHealth(this.parentTower.Damage, this.elementType);
                this.animator.SetTrigger("ImpactOccurred");

                ApplyDebuff();
            }
        }
    }
}
