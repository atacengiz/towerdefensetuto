﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactBehaviour : StateMachineBehaviour
{
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameManager.Instance.ObjectPool.ReleaseObject(animator.gameObject);
    }
}
