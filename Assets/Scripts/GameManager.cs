﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    private TowerButton clickedButton;
    private int currency;
    private int wave = 0;
    private List<Monster> activeMonsters;
    private int lives;
    private bool gameOver = false;
    private Tower selectedTower;
    private int health = 15;
    [SerializeField] private TextMeshProUGUI currencyText;
    [SerializeField] private TextMeshProUGUI waveText;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private GameObject waveButton;
    [SerializeField] private GameObject gameOverMenu;
    [SerializeField] private GameObject upgradePanel;
    [SerializeField] private TextMeshProUGUI sellText;
    [SerializeField] private GameObject statsPanel;
    [SerializeField] private TextMeshProUGUI visualText;
    [SerializeField] private TextMeshProUGUI upgradeText;
    [SerializeField] private GameObject menuPanel;
    [SerializeField] private GameObject optionsPanel;

    public TowerButton ClickedButton { get => clickedButton; }
    public Action<int> CurrencyChanged;
    public int Currency
    {
        get => currency;
        set
        {
            currencyText.text = value.ToString();
            currency = value;
            CurrencyChanged(value);
        }
    }

    public ObjectPool ObjectPool { get; set; }
    public bool waveActive
    {
        get
        {
            return activeMonsters.Count > 0;
        }
    }

    public int Lives
    {
        get => lives;
        set
        {
            this.lives = value;

            if (this.lives <= 0)
            {
                this.lives = 0;
                GameOver();
            }

            this.livesText.text = this.lives.ToString();
        }
    }

    private void Awake()
    {
        this.ObjectPool = GetComponent<ObjectPool>();
    }

    private void Start()
    {
        this.Currency = 50;
        this.Lives = 10;
        this.activeMonsters = new List<Monster>();
    }

    private void Update()
    {
        HandleEscape();
    }

    public void PickTower(TowerButton towerButton)
    {
        DeSelectTower();
        if (this.currency >= towerButton.Price && !waveActive)
        {
            this.clickedButton = towerButton;
            Hover.Instance.Activate(towerButton.Sprite);
        }
    }

    public void BuyTower()
    {
        if (this.currency >= this.clickedButton.Price)
        {
            this.Currency -= this.clickedButton.Price;
            Hover.Instance.DeActivate();
            this.clickedButton = null;
        }
    }

    public void SelectTower(Tower tower)
    {
        if (selectedTower != null)
        {
            selectedTower.Select();
        }

        selectedTower = tower;
        tower.Select();

        sellText.text = "<color=#32CD32>+ </color>" + (selectedTower.Price / 2).ToString() + "<color=#32CD32>$</color>";

        upgradePanel.SetActive(true);
    }

    public void DeSelectTower()
    {
        if (selectedTower != null)
        {
            selectedTower.Select();
        }

        upgradePanel.SetActive(false);
        selectedTower = null;
    }

    public void SellTower()
    {
        if (selectedTower != null)
        {
            Currency += selectedTower.Price / 2;
            selectedTower.GetComponentInParent<Tile>().IsEmpty = true;
            Destroy(selectedTower.transform.parent.gameObject);
            DeSelectTower();
        }
    }

    public void StartWave()
    {
        this.wave++;
        this.waveText.text = "Wave: <color=#32CD32>" + this.wave.ToString() + "</color>";
        this.waveButton.SetActive(false);

        StartCoroutine(SpawnWave());
    }

    public void RemoveMonster(Monster monster)
    {
        activeMonsters.Remove(monster);

        if (!waveActive && !gameOver)
        {
            waveButton.SetActive(true);
        }
    }

    public void GameOver()
    {
        if (!gameOver)
        {
            gameOver = true;
            gameOverMenu.SetActive(true);
        }
    }

    private void HandleEscape()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (selectedTower == null && !Hover.Instance.IsVisible)
            {
                ShowGameMenu();
            }
            else if (Hover.Instance.IsVisible)
            {    
                DropTower();
            }
            else if (selectedTower != null)
            {
                DeSelectTower();
            }
        }
    }

    public void Restart()
    {
        Time.timeScale = 1.0f;

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        Time.timeScale = 1.0f;

        SceneManager.LoadScene(0);
    }

    public void ShowStats()
    {
        statsPanel.SetActive(!statsPanel.activeInHierarchy);
    }

    public void ShowTooltipText(string text)
    {
        visualText.text = text;
    }

    public void UpdateUpgradeTooltip()
    {
        if (selectedTower != null)
        {
            sellText.text = "<color=#32CD32>+ </color>" + (selectedTower.Price / 2).ToString() + "<color=#32CD32>$</color>";
            ShowTooltipText(selectedTower.GetStats());
            
            if (selectedTower.NextUpgrade != null)
            {
                upgradeText.text = "<color=red>- </color>" + selectedTower.NextUpgrade.Price.ToString() + "<color=#32CD32>$</color>";
            }
            else
            {
                upgradeText.text = string.Empty;
            }
        }
    }
    
    public void ShowSelectedTowerStats()
    {
        statsPanel.SetActive(!statsPanel.activeInHierarchy);
        UpdateUpgradeTooltip();
    }

    public void UpgradeTower()
    {
        if (selectedTower != null)
        {
            if (selectedTower.CurrrentTowerLevel <= selectedTower.Upgrades.Length &&
                currency >= selectedTower.NextUpgrade.Price)
            {
                selectedTower.Upgrade();
            }
        }
    }

    public void ResumeGame()
    {
        ShowGameMenu();
    }

    public void ShowGameMenu()
    {
        if (optionsPanel.activeInHierarchy)
        {
            HideOptions();    
        }
        else
        {
            menuPanel.SetActive(!menuPanel.activeInHierarchy);
            if (!menuPanel.activeInHierarchy)
            {
                Time.timeScale = 1.0f;
            }
            else
            {
                Time.timeScale = 0.0f;
            }   
        }
    }

    public void ShowOptions()
    {
        menuPanel.SetActive(false);
        optionsPanel.SetActive(true);
    }
    
    public void HideOptions()
    {
        menuPanel.SetActive(true);
        optionsPanel.SetActive(false);
    }

    private void DropTower()
    {
        clickedButton = null;
        Hover.Instance.DeActivate();
    }

    private IEnumerator SpawnWave()
    {
        LevelManager.Instance.GeneratePath();

        for (int i = 0; i < wave; i++)
        {
            int monsterIndex = UnityEngine.Random.Range(0, 4);
            string monsterType = string.Empty;

            switch (monsterIndex)
            {
                case 0:
                    monsterType = "BlueMonster";
                    break;
                case 1:
                    monsterType = "RedMonster";
                    break;
                case 2:
                    monsterType = "GreenMonster";
                    break;
                case 3:
                    monsterType = "PurpleMonster";
                    break;
            }

            Monster monster = this.ObjectPool.GetObject(monsterType).GetComponent<Monster>();
            monster.Spawn(this.health);

            if (wave % 3 == 0)
            {
                health += 5;
            }

            activeMonsters.Add(monster);

            yield return new WaitForSeconds(2.5f);
        }
    }
}
