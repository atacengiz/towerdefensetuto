﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image foregroundImage;
    private float updateSpeedSeconds = 0.5f;

    private void Awake()
    {
        GetComponentInParent<Monster>().OnHealthChanged += HandleHealthChanged;
    }

    private void HandleHealthChanged(float percentage)
    {
        StartCoroutine(ChangeToPercent(percentage));
    }

    private IEnumerator ChangeToPercent(float percentage)
    {
        float preChangePercent = foregroundImage.fillAmount;
        float elapsed = 0.0f;

        while (elapsed < this.updateSpeedSeconds)
        {
            elapsed += Time.deltaTime;
            foregroundImage.fillAmount = Mathf.Lerp(preChangePercent, percentage, elapsed / this.updateSpeedSeconds);
            yield return null;
        }

        foregroundImage.fillAmount = percentage;
    }
}
