﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : Singleton<SoundManager>
{
    [SerializeField] private AudioSource musicSource;
    [SerializeField] private AudioSource sfxSource;
    [SerializeField] private Slider sfxSlider;
    [SerializeField] private Slider musicSlider;
    
    Dictionary<string, AudioClip> audioClips = new Dictionary<string, AudioClip>();
    
    private void Start()
    {
        AudioClip[] clips = Resources.LoadAll<AudioClip>("Audio");
        foreach (var audioClip in clips)
        {
            audioClips.Add(audioClip.name, audioClip);
        }
        
        LoadVolume();
        
        musicSlider.onValueChanged.AddListener(delegate { UpdateVolume(); });
        sfxSlider.onValueChanged.AddListener(delegate { UpdateVolume(); });
    }

    public void playSFX(string name)
    {
        sfxSource.PlayOneShot(audioClips[name]);
    }

    public void UpdateVolume()
    {
        musicSource.volume = musicSlider.value;
        sfxSource.volume = sfxSlider.value;
        
        PlayerPrefs.SetFloat("SFX", sfxSlider.value);
        PlayerPrefs.SetFloat("Volume", musicSlider.value);
    }

    public void LoadVolume()
    {
        musicSource.volume = PlayerPrefs.GetFloat("Volume", 0.5f);
        sfxSource.volume = PlayerPrefs.GetFloat("SFX", 0.5f);

        musicSlider.value = musicSource.volume;
        sfxSlider.value = sfxSource.volume;
    }
}
