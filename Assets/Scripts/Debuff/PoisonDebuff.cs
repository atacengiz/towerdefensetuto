﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonDebuff : Debuff
{
    private float tickTime;
    private float timeSinceTick;
    private PoisonSplash poisonSplash;
    private float splashDamage;

    public PoisonDebuff(float splashDamage, float tickTime, PoisonSplash poisonSplash, Monster target, float duration) : base(target, duration)
    {
        this.splashDamage = splashDamage;
        this.tickTime = tickTime;
        this.poisonSplash = poisonSplash;
    }

    public override void Update()
    {
        if (target != null)
        {
            timeSinceTick += Time.deltaTime;

            if (timeSinceTick >= tickTime)
            {
                timeSinceTick = 0;

                Splash();
            }
        }

        base.Update();
    }

    private void Splash()
    {
        PoisonSplash splash = GameObject.Instantiate(poisonSplash, target.transform.position, Quaternion.identity);
        splash.Damage = splashDamage;

        Physics2D.IgnoreCollision(target.GetComponent<Collider2D>(), splash.GetComponent<Collider2D>());
    }
}
