﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonSplash : MonoBehaviour
{
    public float Damage { get; set; }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Monster"))
        {
            other.GetComponent<Monster>().ModifyHealth(Damage, Element.POISON);
            Destroy(gameObject);
        }
    }
}
