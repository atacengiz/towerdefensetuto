﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DebugTile : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI fText;
    [SerializeField] private TextMeshProUGUI gText;
    [SerializeField] private TextMeshProUGUI hText;

    public TextMeshProUGUI FText { get => fText; set => fText = value; }
    public TextMeshProUGUI GText { get => gText; set => gText = value; }
    public TextMeshProUGUI HText { get => hText; set => hText = value; }
}
