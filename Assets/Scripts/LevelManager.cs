﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    public Dictionary<Point, Tile> Tiles { get; set; }
    public Portal GreenPortal { get; set; }
    public Stack<Node> Path
    {
        get
        {
            if (this.path == null)
            {
                GeneratePath();
            }
            return new Stack<Node>(new Stack<Node>(path));
        }
    }

    public Bounds TileSize
    {
        get
        {
            SpriteRenderer spriteRenderer = tilePrefabs[1].GetComponent<SpriteRenderer>();
            return spriteRenderer.sprite.bounds;
        }

    }

    public Point GreenSpawnPoint { get => greenSpawnPoint; }

    public Point PurpleSpawnPoint
    {
        get { return purpleSpawnPoint; }
    }

    [SerializeField] private GameObject[] tilePrefabs;
    [SerializeField] private CameraController cameraController;
    [SerializeField] private GameObject greenPortal;
    [SerializeField] private GameObject purplePortal;
    [SerializeField] private GameObject map;
    private Point mapSize;
    private Point greenSpawnPoint;
    private Point purpleSpawnPoint;
    private Stack<Node> path;

    private void Start()
    {
        CreateLevel();
    }

    private void CreateLevel()
    {
        Tiles = new Dictionary<Point, Tile>();
        string[] mapData = ReadLevelText();

        Vector3 worldStart = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, -10));
        int mapX = mapData[0].ToCharArray().Length;
        int mapY = mapData.Length;

        this.mapSize = new Point(mapX, mapY);
        Vector3 maxTile = Vector3.zero;

        for (int y = 0; y < mapY; y++)
        {
            char[] newTiles = mapData[y].ToCharArray();

            for (int x = 0; x < mapX; x++)
            {
                PlaceTile(newTiles[x].ToString(), x, y, worldStart);
            }
        }

        maxTile = Tiles[new Point(mapX - 1, mapY - 1)].transform.position;
        cameraController.SetLimits(new Vector3(maxTile.x + TileSize.size.x, maxTile.y - TileSize.size.y));

        SpawnPortals();
    }

    private void PlaceTile(string tileType, int columnNumber, int rowNumber, Vector3 worldStartPosition)
    {
        int tilePrefabIndex = int.Parse(tileType);

        Tile newTile = Instantiate(tilePrefabs[tilePrefabIndex]).GetComponent<Tile>();
        newTile.Setup(new Point(columnNumber, rowNumber), newTile.transform.position = new Vector3(worldStartPosition.x + (TileSize.size.x * columnNumber), worldStartPosition.y - (TileSize.size.y * rowNumber)), map.transform);
    }

    private string[] ReadLevelText()
    {
        TextAsset bindData = Resources.Load("Level") as TextAsset;
        string data = bindData.text.Replace(Environment.NewLine, string.Empty);
        return data.Split('-');
    }

    private void SpawnPortals()
    {
        this.greenSpawnPoint = new Point(0, 0);
        GameObject tempGreenPortal = Instantiate(greenPortal, Tiles[GreenSpawnPoint].WorldPosition, Quaternion.identity);
        this.GreenPortal = tempGreenPortal.GetComponent<Portal>();
        this.GreenPortal.name = "GreenPortal";

        purpleSpawnPoint = new Point(11, 6);
        Instantiate(purplePortal, Tiles[purpleSpawnPoint].WorldPosition, Quaternion.identity);
    }

    public bool InBounds(Point position)
    {
        return position.X >= 0 && position.Y >= 0 && position.X < mapSize.X && position.Y < mapSize.Y;
    }

    public void GeneratePath()
    {
        this.path = AStar.GetPath(this.GreenSpawnPoint, this.purpleSpawnPoint);
    }
}