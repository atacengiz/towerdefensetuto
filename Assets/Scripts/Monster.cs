﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    public Point GridPosition { get; set; }
    public bool IsActive { get; set; }
    public event Action<float> OnHealthChanged = delegate { };
    public bool IsAlive => this.currentHealth > 0;

    public Element ElementType { get => elementType; }
    public float Speed { get => speed; set => speed = value; }
    public float MaxSpeed { get; set; }

    [SerializeField] private float speed;
    private int maxHealth;
    private Stack<Node> path;
    private Vector3 destination;
    private Animator animator;
    private float currentHealth;
    private SpriteRenderer renderer;
    private int invulnerability = 2;
    [SerializeField] private Element elementType;
    private List<Debuff> debuffs;
    private List<Debuff> debuffsToRemove;
    private List<Debuff> debuffsToAdd;

    private void Awake()
    {
        this.IsActive = false;
        this.animator = GetComponent<Animator>();
        this.renderer = GetComponent<SpriteRenderer>();
        this.debuffs = new List<Debuff>();
        this.debuffsToRemove = new List<Debuff>();
        this.debuffsToAdd = new List<Debuff>();
        this.MaxSpeed = this.speed;
    }

    private void Update()
    {
        HandleDebuffs();
        Move();
    }

    private void Move()
    {
        if (!this.IsActive)
        {
            return;
        }

        this.animator.SetBool("IsWalking", true);
        transform.position = Vector2.MoveTowards(transform.position, destination, speed * Time.deltaTime);

        if (transform.position == destination)
        {
            SetPath(path);
        }
    }

    private void SetPath(Stack<Node> newPath)
    {
        if (newPath != null && newPath.Count > 0)
        {
            this.path = newPath;
            this.GridPosition = path.Peek().GridPosition;
            this.destination = path.Pop().WorldPosition;
        }
    }

    public void Spawn(int health)
    {
        transform.position = LevelManager.Instance.GreenPortal.transform.position;
        SetPath(LevelManager.Instance.Path);

        this.maxHealth = health;
        this.currentHealth = maxHealth;

        this.OnHealthChanged(1.00f);

        StartCoroutine(Scale(new Vector3(0.1f, 0.1f), new Vector3(1.0f, 1.0f), false));
    }

    public IEnumerator Scale(Vector3 from, Vector3 to, bool shouldDestroy)
    {
        float progress = 0.0f;

        while (progress <= 1.0f)
        {
            transform.localScale = Vector3.Lerp(from, to, progress);
            progress += Time.deltaTime;
            yield return null;
        }

        transform.localScale = to;

        if (shouldDestroy)
        {
            Release();
        }
        else
        {
            this.IsActive = true;
        }
    }

    public void ModifyHealth(float damage, Element damageSource)
    {
        if (!this.IsActive)
            return;

        if (damageSource == this.elementType)
        {
            damage = damage / this.invulnerability;
            this.invulnerability++;
        }

        this.currentHealth -= damage;

        if (this.currentHealth <= 0)
        {
            SoundManager.Instance.playSFX("Death");
            
            this.currentHealth = 0;
            GameManager.Instance.Currency += 2;
            this.animator.SetTrigger("DeathTrigger");

            this.IsActive = false;

            GetComponent<SpriteRenderer>().sortingOrder--;
        }

        float percent = (float)this.currentHealth / (float)this.maxHealth;
        OnHealthChanged(percent);
    }

    public void Release()
    {
        this.IsActive = false;
        this.GridPosition = LevelManager.Instance.GreenSpawnPoint;
        this.debuffs.Clear();
        this.debuffsToAdd.Clear();
        this.debuffsToRemove.Clear();
        GameManager.Instance.ObjectPool.ReleaseObject(gameObject);
        GameManager.Instance.RemoveMonster(this);
    }

    public void AddDebuff(Debuff debuff)
    {
        if (debuffs.Exists(t => t.GetType() == debuff.GetType()))
        {
            return;
        }
        else
        {
            debuffsToAdd.Add(debuff);
        }
    }

    public void RemoveDebuff(Debuff debuff)
    {
        debuffsToRemove.Remove(debuff);
    }

    private void HandleDebuffs()
    {
        if(debuffsToAdd.Count > 0)
        {
            debuffs.AddRange(debuffsToAdd);
            debuffsToAdd.Clear();
        }

        foreach(Debuff debuff in debuffsToRemove)
        {
            debuffs.Remove(debuff);
            debuffsToRemove.Clear();
        }

        foreach(Debuff debuff in debuffs)
        {
            debuff.Update();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("PurplePortal"))
        {
            StartCoroutine(Scale(new Vector3(1.0f, 1.0f), new Vector3(0.1f, 0.1f), true));
            GameManager.Instance.Lives--;
        }
        else if (other.CompareTag("Tile"))
        {
            renderer.sortingOrder = other.GetComponent<Tile>().GridPosition.Y;
        }
    }
}
