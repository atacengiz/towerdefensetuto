﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject optionsMenu;
    
    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    public void Options()
    {
        if (optionsMenu.activeInHierarchy)
        {
            optionsMenu.SetActive(false);
            mainMenu.SetActive(true);
        }
        else
        {
            optionsMenu.SetActive(true);
            mainMenu.SetActive(false);
        }
    }

    public void Quit()
    {
        Application.Quit();
    }
}
